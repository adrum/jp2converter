# README

## convert_to_jp2


MAINTAINER Alex Drummer <drummerroma@gmail.com>

Simple Dockerfile that you can use to create a Docker image containing the script and the necessary dependencies (ImageMagick). Save this Dockerfile in the same directory as your script, and then build the Docker image.

This assumes that you want to mount the input and output directories into the Docker container. Adjust the paths accordingly based on your directory structure and requirements.

This Docker image uses Alpine Linux as a base image for its lightweight nature, and it installs ImageMagick as required by your script. The entry point is set to your script, and the Docker container expects two arguments: the input directory and the output directory.
The default input_dir and output_dir within the Docker image are set to /input and /output, respectively. If you run the container without specifying input and output directories, it will use these defaults.
Get supported extensions from the environment variable SUPPORTED_EXTENSIONS, defaulting to "jpg|jpeg|JPG"


#### Build Image

```bash
docker build -t convert-to-jp2 .
```

Environment variable:
- SUPPORTED_EXTENSIONS="jpg|jpeg|png"


#### Run the image

```bash
docker run -e SUPPORTED_EXTENSIONS="jpg|jpeg|png" -v /absolute/path/on/host/to/your/input_directory:/input -v /absolute/path/on/host/to/your/output_directory:/output jpg-to-jp2-converter {optional:  /input /output}
```

The default input_dir and output_dir within the Docker image are set to /input and /output, respectively. If you run the container without specifying input and output directories, it will use these defaults.

Make sure your script (convert_to_jp2.sh) is in the same directory as the Dockerfile.

