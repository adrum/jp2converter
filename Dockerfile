# Use a lightweight base image with ImageMagick installed
FROM alpine:latest

# Install ImageMagick
RUN apk --no-cache add imagemagick

# Set the working directory
WORKDIR /app

# Copy the script into the container
COPY convert_to_jp2.sh /app/convert_to_jp2.sh

# Make the script executable
RUN chmod +x /app/convert_to_jp2.sh

# Define the entry point for the container
ENTRYPOINT ["/app/convert_to_jp2.sh"]
