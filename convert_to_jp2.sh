#!/bin/bash

# Set default input and output directories within the Docker image
input_dir="/input"
output_dir="/output"

# Function to convert supported image formats to JP2
convert_to_jp2() {
  input_file="$1"
  output_file="$2/$(basename "$input_file" | sed 's/\.[^.]*$//').jp2"

  echo "Converting $input_file to $output_file"

  # Use ImageMagick to convert image to JP2
  convert "$input_file" "$output_file"
}

# Recursive function to process files in a directory
process_files() {
  local input_dir="$1"
  local output_base_dir="$2"
  local supported_extensions="$3"

  # Create the corresponding output directory
  local output_dir="$output_base_dir/$(basename "$input_dir")"
  mkdir -p "$output_dir"

  # Process files in the current directory
  for file in "$input_dir"/*; do
    if [ -d "$file" ]; then
      # If it's a directory, recursively process its contents
      process_files "$file" "$output_dir" "$supported_extensions"
    elif [[ "$file" == *.$supported_extensions ]]; then
      # If it's a supported image file, convert it to JP2
      convert_to_jp2 "$file" "$output_dir"
    fi
  done
}

# Check if ImageMagick is installed
if ! command -v convert &> /dev/null; then
  echo "ImageMagick is not installed. Please install it and try again."
  exit 1
fi

# Get supported extensions from the environment variable, defaulting to "jpg|jpeg|JPG"
supported_extensions="${SUPPORTED_EXTENSIONS:-jpg|jpeg|JPG}"

# Start the conversion process
process_files "$input_dir" "$output_dir" "$supported_extensions"

echo "Conversion complete. JP2 files saved in $output_dir."
